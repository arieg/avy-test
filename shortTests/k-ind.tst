c RUN: bash -c "%avy %OPT1 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT3 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT4 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT5 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT6 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT7 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT8 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT9 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c CHECK: ^BRUNCH_STAT Result UNSAT$
verilog code for the counter circuit 
module kind (
  input clk,
  output  x,
  output  a,
  output  b,
  output  c
);
  reg  x = 1;
  reg  a = 1;
  reg  b = 1;
  reg  c = 1;

  always @(posedge clk) begin
    x=a|b|c;
    a=b|a;
    b=c;
    c=0;
  end

  assert property (x == 1 );
endmodule
c RUN: bash -c "%avy %OPTabs %p/$(basename %s tst)aig" | OutputCheck %REM %s
