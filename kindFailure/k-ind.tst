c RUN: bash -c "%avy %OPTkind %p/$(basename %s tst)aig" | OutputCheck %REM %s
c CHECK: ^BRUNCH_STAT Result UNSAT$
verilog code for the counter circuit 
module kind (
  input clk,
  output  x,
  output  a,
  output  b,
  output  c
);
  reg  x = 1;
  reg  a = 1;
  reg  b = 1;
  reg  c = 1;

  always @(posedge clk) begin
    x=a|b|c;
    a=b|a;
    b=c;
    c=0;
  end

  assert property (x == 1 );
endmodule
