# Test suite for Avy

This is a lit-based test suite for Avy. It requires `lit` and `OutputCheck`.
These can be installed using

``` bash
$ pip install lit OutputCheck
```

# Running test suite

To run all tests use the following command

``` bash
$ lit --path=PATH_TO_AVY TEST_DIR
```

where `PATH_TO_AVY` is the path for `avy` executable being tested, and
`TEST_DIR` is either top-level directory containing this file, or any
of the sub-directories.


# Defining a test

Each test consists of two files: `NAME.tst` that defines the test
commands, and `NAME.aig` that contains the AIG to be used as input.
An example is provided in the `test` sub-directory.

Additional substitutions (variables named with `%`-symbol in
`.tst` file) can be defined in `lit.cfg`.

# CMake support

TODO. For now, tests have to be run by hand using the `lit` command as
described above.

## Contributors

Arie Gurfinkel <arie.gurfinkel@uwaterloo>
