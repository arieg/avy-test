c RUN: bash -c "%avy %OPT1 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT3 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT4 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT5 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT6 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT7 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT8 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT9 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c CHECK: ^BRUNCH_STAT Result UNSAT$
instance from hwmcc17
expect convergence in 7 frames 
c RUN: bash -c "%avy %OPTabs %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTkind %p/$(basename %s tst)aig" | OutputCheck %REM %s
