c RUN: bash -c "%avy %OPT2 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %itp-simp-minisat %p/$(basename %s tst)aig" | OutputCheck %REM %s
c CHECK: ^BRUNCH_STAT Result UNSAT$
verilog code for the counter circuit 
The property is 2-inductive
module demo (
  input clk,
  output [5:0] counter //there are 6 latches
);
  reg [5:0] counter = 0; //initialize with 0

  always @(posedge clk) begin
    if (counter == 10)
      counter <= 0; //assign the value 0
    else
      counter <= counter + 1;
  end

  assert property (counter <= 11 ); // check less than or equal to 11
endmodule
c RUN: bash -c "%avy %OPT${$1} %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT""$1"" %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTm %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTabs %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTkind %p/$(basename %s tst)aig" | OutputCheck %REM %s
