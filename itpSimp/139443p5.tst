c RUN: bash -c "%avy %OPT2 %p/$(basename %s tst)aig; exit 0" | OutputCheck %REM %s
c RUN: bash -c "%avy %itp-simp-minisat %p/$(basename %s tst)aig; exit 0" | OutputCheck %REM %s
c CHECK: ^BRUNCH_STAT Result SAT$
instance from hwmcc17
expect result in 3 frames 
c RUN: bash -c "%avy %OPT${$1} %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT""$1"" %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTm %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTabs %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTkind %p/$(basename %s tst)aig" | OutputCheck %REM %s
