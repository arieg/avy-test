# Interpolation with Simplification 

These tests check combination of interpolation with simplification
enabled in the SAT solver. They have exposed bugs in our implementation
in Glucose and Minisat. 

The set might be reduced since it identified mostly the same problems:
  (a) not all clauses on the proof trail are learnt 
  (b) duplicate core clauses can mask one another
  (c) clauses db might contain deleted clauses just like learnt db can

