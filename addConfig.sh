#!/bin/bash
#add a new configuration to all tst files
#usage ./addConfig <nameOfConfig>
for f in */*.tst
do
 echo "" >> "$f"
 echo 'c RUN: bash -c "%avy %OPT'$1' %p/$(basename %s tst)aig" | OutputCheck %REM %s' >> "$f"
done 
